const prodPlugins = process.env.NODE_ENV === 'production' ? ['transform-remove-console'] : []
module.exports = {
  plugins: [
    ...prodPlugins
  ],
  presets: [
    '@vue/cli-plugin-babel/preset'
  ]
}


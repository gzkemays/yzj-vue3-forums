# 椰子鸡论坛第三版

> ​	基于第二版的 https://gitee.com/gzkemays/yzj-forum.git 的页面布局下进行优化，并且采取 Vue3 + Typescript 进行编写。【第二版虽然用了 Vue3 但是由于当时对 Vue3 的使用还不够了解，导致代码结构很乱】

- 本地测试运行

```
npm run dev
```

- 项目打包

 打包已经采用 `Webpack` 进行压缩，同目录下会生成 `report.html` 提供参考。

```
npm run build
```

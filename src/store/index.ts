import {createStore} from 'vuex'
import User from './modules/User';
import Socket from './modules/Socket';

export default createStore({
  modules: {
    User,
    Socket
  }
})

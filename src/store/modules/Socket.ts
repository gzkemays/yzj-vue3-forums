import {LoginForm} from "@/interface/user/entity/UserEntityInterface";
import {NoticeMessage} from '@/interface/websocket/entity/SocketMessage';

const SAVE_CURRENT_ONLINE = 'saveCurrentOnline', SAVE_NOTICES = "saveNotices";
export default {
  namespaced: true,
  /** 基本对象 */
  state: {
    /** 在线人数 */
    online: 0,
    /** 消息通知 */
    notices: {
      /** 全部消息 */
      allNoticeMessages: [],
      /** 系统消息 */
      systemNoticeMessages: [],
      /** 聊天消息 */
      chatNoticeMessages: [],
      /** 文章消息 */
      detailNoticeMessages: []
    }
  },
  /** getter */
  getters: {
    currentOnline(state: any) {
      return state.online
    },
    getNotices(state: any) {
      return state.notices;
    }
  },
  /** 同步函数 */
  mutations: {
    /** 保存当前在线人数 */
    saveCurrentOnline(state: any, count: number) {
      state.online = count;
    },
    /** 保存列表消息 */
    saveNotices(state: any, notices: any) {
      state.notices.allNoticeMessages = notices.allNoticeMessages;
      state.notices.systemNoticeMessages = notices.systemNoticeMessages;
      state.notices.chatNoticeMessages = notices.chatNoticeMessages;
      state.notices.detailNoticeMessages = notices.detailNoticeMessages;
    }
  },
  /** 异步函数 */
  actions: {
    saveCurrentOnline({commit}: any, count: number) {
      commit(SAVE_CURRENT_ONLINE, count);
    },
    saveNotices({commit}: any, notices: any) {
      commit(SAVE_NOTICES, notices);
    }
  }
}
import {createRouter, createWebHashHistory, RouteRecordRaw} from 'vue-router'
import HomeView from '../views/HomeView.vue'
import EditorView from '../views/EditorView.vue';
import DetailView from '../views/DetailView.vue';
import FileView from '../views/FileView.vue';
import DocumentationView from '../views/DocumentationView.vue';
import FeedbackView from '../views/FeedbackView.vue';
import ChatView from '../views/ChatView.vue';
import ShareLinkView from '../views/ShareLinkView.vue';
import FriendlyLinkView from '../views/FriendlyLinkView.vue';
import QAndAView from '../views/QAndAView.vue';
import DocumentationReadView from '../views/DocumentationReadView.vue';
import GuestbookView from '../views/GuestbookView.vue';
import QAndADetailView from '../views/QAndADetailView.vue';
import PersonalCentreView from '../views/PersonalCentreView.vue';
import InterviewsView from '../views/InterviewsView.vue';
const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    meta: {
      title: "欢迎来到椰子鸡大家庭首页(●'◡'●)"
    },
    component: HomeView
  },
  {
    path: '/interviews',
    name: 'interviewsView',
    meta: {
      title: "分享自己下自己面试所遇到的问题吧(●'◡'●)"
    },
    component: InterviewsView
  },
  {
    path: '/edit',
    name: 'edit',
    meta: {
      requiredLogin: true,
      title: "好期待你的文章哦~~(￣▽￣)~~*"
    },
    component: EditorView
  },
  /** 有参 centre 跳转为其他人的个人中心，无参时跳转至自己的个人中心 */
  {
    path: '/centre/:nickname?',
    name: 'centre',
    component: PersonalCentreView
  },
  {
    path: '/detail/:id',
    name: 'detail',
    component: DetailView
  },
  {
    path: '/commonFile',
    name: 'file',
    meta: {
      title: "独乐乐不如众乐乐~"
    },
    component: FileView
  },
  {
    path: '/documentation',
    name: 'documentation',
    meta: {
      title: "希望这里有你喜欢的文档呢 (￣▽￣)"
    },
    component: DocumentationView
  },
  {
    path: '/read',
    name: 'read',
    meta: {
      requiredLogin: true
    },
    component: DocumentationReadView
  },
  {
    path: '/feedback',
    name: 'feedbackView',
    meta: {
      title: "反馈的内容会让大家看到哦~当然开发者也会看到呢！"
    },
    component: FeedbackView
  },
  {
    path: '/chat',
    name: 'chatView',
    meta: {
      requiredLogin: true,
      title: "谨言慎行🤫"
    },
    component: ChatView
  },
  {
    path: '/share',
    name: 'shareLinkView',
    meta: {
      title: "你会和大家一起分享自己喜欢的网站吗？(^人^)"
    },
    component: ShareLinkView
  },
  {
    path: '/friendly',
    name: 'friendlyLinkView',
    meta: {
      title: "这里都是开发者推荐的网站哦(●'◡'●)"
    },
    component: FriendlyLinkView
  },
  {
    path: '/q&a',
    name: 'qAndAView',
    meta: {
      title: "有问题就来这里也许会有好心大佬帮你......的吧 (￣▽￣)"
    },
    component: QAndAView
  },
  {
    path: '/guestbook',
    name: 'guestbookView',
    meta: {
      title: "躁起来，记得登录才能永久记录哦~"
    },
    component: GuestbookView
  },
  {
    path: '/q&a/:id',
    name: 'q&a',
    component: QAndADetailView
  }
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes
})

export default router

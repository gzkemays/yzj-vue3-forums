import app from '@/main';

interface ToastShow {
    success: (title: string, context?: string | null | undefined, life?: number | null | undefined) => void;
    error: (title: string, context?: string | null | undefined, life?: number | null | undefined) => void;
    warn: (title: string, context?: string | null | undefined, life?: number | null | undefined) => void;
    info: (title: string, context?: string | null | undefined, life?: number | null | undefined) => void;
}

function getToast(severity: string, summary: string, detail: string | null | undefined, life: number | null | undefined) {
    // 通过 app globalProperties 获取 toast
    let toast = app.config.globalProperties.$toast;
    if (detail && life) {
        toast.add({severity: severity, summary: summary, detail: detail, life: life});
    } else if (!detail && life) {
        toast.add({severity: severity, summary: summary, life: life});
    } else if (detail && !life) {
        toast.add({severity: severity, summary: summary, detail: detail, life: 1500});
    } else {
        toast.add({severity: severity, summary: summary, life: 1500});
    }
}

const toast: ToastShow = {
    success: (title: string, context: string | null | undefined, life: number | null | undefined) => {
        getToast('success', title, context, life);
    },
    error: (title: string, context: string | null | undefined, life: number | null | undefined) => {
        getToast('error', title, context, life);
    },
    warn: (title: string, context: string | null | undefined, life: number | null | undefined) => {
        getToast('warn', title, context, life);
    },
    info: (title: string, context: string | null | undefined, life: number | null | undefined) => {
        getToast('info', title, context, life);
    },
}

export default toast;
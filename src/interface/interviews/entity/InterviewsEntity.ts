export interface InterviewsInfo {
  title: string
  type: string
  answer: string
}
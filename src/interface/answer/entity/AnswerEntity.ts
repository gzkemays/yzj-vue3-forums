/** 回答响应数据 */
export interface AnswerResponse {
    id: number, // 7,
    nickname: string, // "gzkemays",
    atNickname: string, // "78773910",
    avatar: string, // "https://img.yeziji.xyz/user/avatar/6772f14a204736c2d8c55bbe1dc36869.jpg",
    desc: string, // "好好努力，下好每一顿饭。",
    content: string, // "你说得对",
    goodNumber: number, // null,
    toOpera: boolean, // 是否点赞
    child: AnswerResponse[], // null,
    createTime: string, // "2022-11-14 17:13:08",
    updateTime: string, // "2022-11-14 17:13:15"
}

/**
 * 回复时必须带的参数
 *
 * 0 回复问题 | 1 回复答案
 */
export type ReplyMode = 0 | 1;

/** 回复的信息体 */
export interface AnswerReply {
    mode: ReplyMode
    id: number
    content: string
}
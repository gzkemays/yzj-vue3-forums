export interface DetailSpecial {
  id: number
  title: string
  desc: string
}

/** 文章响应数据 */
export interface DetailResponseData {
  /** 作者 */
  author: string,
  /** 是否格式化 */
  autoFormat: boolean
  /** 作者头像 */
  avatar: string
  /** 作者简介 */
  authorDesc: string
  /** 综合评分 */
  aveRate: number
  /** 收藏数量 */
  collectionNumber: number
  /** 评论数量 */
  commentNumber: number
  /** 文本内容需要 gzip 解压 */
  content: string
  /** 简介 */
  desc: string
  /** 点赞数量 */
  goodNumber: number
  /** 文章 id */
  id: number
  /** 发布时间 */
  publishTime: string
  /** 文章序列号 */
  serialNumber: number
  /** 文章专栏 */
  special: string
  /** 文章标签 */
  tags: string
  /** 是否为草稿 */
  tempSave: boolean
  /** 文章标题 */
  title: string
  /** 用户是否收藏 */
  toCollection?: boolean
  /** 用户是否评论 */
  toComment?: boolean
  /** 用户是否点赞 */
  toGood?: boolean
  /** 用户是否评分 */
  toRate: number
  /** 文章是否可以恢复 -- 一般用户删除后该值为 true */
  toRecovery?: boolean
  /** 发布文章的作者 id */
  userId: number
  /** 文章浏览数量 */
  view: number
  /**
   * 相关专栏列表
   */
  specialVOS: DetailSpecial[]
}

/** 文章分页请求信息体 */
export interface DetailPageInfo {
  /** 当前页码 */
  page: number
  /** 截取的数据范围 */
  limit: number
  /** 总页数 */
  totalPage?: number,
  /** 数据总量 */
  total?: number,
  /** 用户名 */
  nickname?: string
  /** 搜索内容 */
  title?: string
  /** 搜索标签 */
  tags?: string
  /** 搜索模式 */
  mode?: number
  /** 是否为草稿 */
  tempSave?: boolean
  /** 是否可用 */
  delete?: boolean
  /** 是否为收藏 */
  collection?: boolean
  /** 是否为点赞 */
  good?: boolean
}

/** 单篇文章 */
export interface DetailInfo {
  /** 文章ID */
  id: bigint,
  /** 是否暂存，暂存需要指定 token 才能获取 */
  tempSave: boolean;
}

/** 发布文章 */
export interface DetailPublish {
  /** id */
  id: number
  /** 标题 */
  title: string,
  /** 文章内容 */
  content: string,
  /** 标签 */
  tags: string,
  /** 临时存储 */
  tempSave: boolean,
  /** 简介 */
  desc: string,
  /** 专栏 */
  special: string,
  /** 是否删除，删除或恢复时使用 */
  delete?: boolean
}
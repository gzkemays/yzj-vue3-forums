import {StoreNamespace} from '@/utils/KeysProperties';
import {LoginForm, UserMsg} from '../entity/UserEntityInterface';
import {UserService} from '@/utils/ApiRequest';

const KEY = {
  /** 用户信息 */
  USER_MSG: 'userMsg',
  /** 用户登录 */
  LOGIN: 'login',
  /** 用户登出 */
  LOGOUT: 'logOut',
  /** 登录状态 */
  LOGIN_STATUS: 'loginStatus',
  /** 保存用户总数 */
  GET_USER_COUNT: 'getUserCount',
  /** 用户总数 */
  USER_COUNT: 'userCount'
}

interface UserStore {
  /** 登录 */
  login: (loginFrom: LoginForm) => void;
  /** 获取用户消息 */
  getUserMsg: () => UserMsg;
  /** 获取登录状态 */
  getLoginStatus: () => boolean;
  /** 判断 usermsg 是否可用 */
  validateEmpty: (userMsg: UserMsg) => boolean;
  /** 获取成员总数 */
  getUserCount: () => number;
  /** 保存成员总数 */
  saveUserCount: () => Promise<number>;
}

export default class UserStoreImpl implements UserStore {
  namespace = new StoreNamespace('User');
  userService = new UserService();

  /** 登录 */
  async login(loginFrom: LoginForm) {
    return await this.namespace.dispatch(KEY.LOGIN, loginFrom);
  }

  /** 登出 */
  async logOut() {
    return await this.namespace.dispatch(KEY.LOGOUT);
  }

  /** 获取用户信息 */
  getUserMsg = () => this.namespace.getters(KEY.USER_MSG);
  /** 获取用户登录状态 */
  getLoginStatus = () => {
    return this.namespace.getters(KEY.LOGIN_STATUS)
  };
  /** 判断是否为空 */
  validateEmpty = (userMsg: UserMsg) => {
    let hasName = userMsg.username && userMsg.nickname;
    if (hasName) {
      return userMsg.username !== '' && userMsg.nickname !== '';
    }
    return false;
  };
  /**保存成员总数*/
  saveUserCount = async () => {
    return await this.namespace.dispatch(KEY.GET_USER_COUNT);
  }
  /** 获取用户 count */
  getUserCount = () => {
    return this.namespace.getters(KEY.GET_USER_COUNT);
  }
}

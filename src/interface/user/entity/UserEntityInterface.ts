/** 用户点赞\评分\收藏文章 */
export interface UserOperationDetail {
  /** 文章 id */
  detailId: number,
  /** 是否点赞 */
  toGood?: boolean,
  /** 是否收藏 */
  toCollection?: boolean,
  /** 评分 */
  toRate?: string | number
}

/** 登录表单 */
export interface LoginForm {
  /** 用户名 */
  username: string,
  /** 密码 */
  password: string
}

/** 注册表单 */
export interface RegisterForm {
  /** 用户名 */
  username: string,
  /** 密码 */
  password: string,
  /** 确认密码 */
  truthPassword: string,
  /** 邮箱前缀 */
  emailPrefix?: string,
  /**  邮箱后缀 */
  emailSuffix?: string,
  /** 电子邮箱由前后缀组建 */
  email?: string,
  /** 电子邮箱验证码 */
  code: string,
}

/** 用户登录响应 */
export interface UserMsg {
  /** 头像 */
  avatar: string,
  /** 用户简介 */
  desc: string,
  /** 电子邮箱 */
  email: string,
  /** 等级 */
  level: number,
  /** 昵称 */
  nickname: string,
  /** 用户账号 */
  username?: string
  /** 是否关注 */
  focus?: boolean
  /** 是否为粉丝 */
  fans?: boolean
  /** 发布文章次数 */
  detailCount?: number
  /** 提问问题数量 */
  questionCount?: number
  /** 回答问题次数 */
  answerCount?: number
  /** 上传文件次数 */
  fileCount?: number
  /** 粉丝数量 */
  fansCount?: number
  /**
   * 关注数量
   */
  focusCount?: number
  /**注册时间*/
  registerTime?: string
  /** 最后的发言*/
  lastChat?: string
  /** 最后发言的时间*/
  lastChatDate?: string
  /** 通知数量 */
  noticeNum?: number
}

/**
 * 用户的关注列表或粉丝列表分页
 */
export interface UserFocusOrFansPaging {
  page: number
  limit: 10
  nickname: string
  fans: boolean
  focus: boolean
}

/**
 * 用户的关注列表和粉丝列表分页
 */
export interface UserFocusAndFansPaging {
  page: number
  limit: number
  nickname: string
}

/** 更新用户信息 */
export interface UserMsgUpdateInfo {
  userId?: number
  avatar?: string
  desc?: string
  nickname?: string
  focus?: string
  focusIds?: string
  toFocus?: boolean
  file?: File
}

/**
 * 更新回调
 */
export interface UserMsgUpdateResponse {
  avatar: string
  desc: string
  nickname: string
  toFocus: boolean
}

/** 用户一星期的文章统计 */
export interface UserCentre7DaysStatics {
  detailStatics: any
  questionStatics: any
  answerStatics: any
}

/** Vuex UserMsg */
export class UserMsgStore implements UserMsg {
  avatar: string;
  desc: string;
  email: string;
  level: number;
  nickname: string;
  username?: string;

  /** 携参 */
  constructor(userMsg?: UserMsg) {
    if (userMsg) {
      this.avatar = userMsg.avatar;
      this.desc = userMsg.desc;
      this.email = userMsg.email;
      this.level = userMsg.level;
      this.nickname = userMsg.nickname;
      this.username = userMsg.username;
    } else {
      this.avatar = '';
      this.desc = '';
      this.email = '';
      this.level = 0;
      this.nickname = '';
      this.username = '';
    }
  }

  isEmpty() {
    return !this.username || !this.nickname || this.username === '' || this.nickname === '';
  }
}
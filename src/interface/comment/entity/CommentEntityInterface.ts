/**
 * 评论基本信息
 */
export interface Comment {
  id: number //  130,
  userId: number //  21,
  avatar: string //  "https://img.yeziji.xyz/user/avatar/916d6b1c410a84cf30c6f7bf72201fb2.gif",
  nickname: string //  "alone",
  at: string //  null,
  content: string //  "111",
  createTime: string //  "2022-04-24 15:31:58",
  follow: boolean //  0,
  goodNumber: string //  0,
  parentCommentId: string //  0,
  toGood: string //  null,
}

/**
 * 文章评论
 */
export interface DetailComments {
  id: number
  userId: number
  avatar: string
  nickname: string
  at: string
  content: string
  createTime: string
  follow: boolean
  goodNumber: number
  parentCommentId: string
  toGood: boolean
  detailCommentVos: DetailComments[]
}

/** 在文章中评论 */
export interface DetailCommentInfo {
  detailId: number
  // parentCommentId 为 null 时就是评论文章
  parentCommentId?: number
  content: string
}

/** 点赞评论 */
export interface KudosComment {
  commentId: number
  toGood?: boolean
}
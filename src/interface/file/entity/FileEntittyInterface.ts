/** 文件对象接口 */

/** 请求返回对象 */
export interface FileResponseEntity {
  /** 文件名 */
  fileName: string
  /** 文件大小 */
  fileSize: string
  /** 文件上传者 */
  source: string
  /** 文件简介 */
  desc: string
  /** 文件上传时间 */
  updateTime: string
}

/** 文件下载响应 */
export interface FileDownloadResponseEntity {
  /** 文件名 */
  fileName: string
  /** 文件大小 */
  fileSize: string
  /** 文件动态下载地址 */
  accessUrl: string
  /** 文件上传时间 */
  updateTime: string
}

/** 数据文件响应对象 */
export interface FileDataResponseEntity {
  /** 文件名 */
  filename: string
  /** 上传者 */
  source: string
  /** 文件目标地址 */
  target: string
  /** 文件缩略图 */
  image: string
  /** 文件上传时间 */
  createTime: string
}

/** 文件上传渲染对象 */
export interface FileUploadRender {
  /** 是否允许上传 */
  open: boolean
  /** 当前状态: -1 计算阶段 | 0 等待上传 | 1 上传中 | 2 上传完毕 */
  status: -1 | 0 | 1 | 2
  /** 上传进度 */
  progress?: number
  /** 文件 MD5 */
  md5?: string
  /** svg 图标 */
  svg?: string
  /** 文件名 */
  filename?: string
  /** 文件大小 */
  size?: string
  /** 提示文本 */
  text?: string
  /** 提示颜色 */
  color?: 'info' | 'success' | 'warning' | 'danger' | undefined
  /** 文件流 */
  file?: File
}
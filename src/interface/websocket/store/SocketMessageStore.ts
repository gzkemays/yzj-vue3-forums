import {StoreNamespace} from '@/utils/KeysProperties';
import {LoginForm, UserMsg} from "@/interface/user/entity/UserEntityInterface";
import {NoticeMessage} from '../entity/SocketMessage';

const KEY = {
  /** 保存在线用户数量 */
  DISPATCH_SAVE_CURRENT_ONLINE: 'saveCurrentOnline',
  GETTER_ONLINE_KEY: 'currentOnline',
  STATE_ONLINE_KEY: 'online',
  /** 保存通知消息 */
  DISPATCH_SAVE_NOTICES_KEY: 'saveNotices',
  GETTER_NOTICES_KEY: 'getNotices',
  STATE_NOTICES_KEY: 'notices'
}

interface SocketMessageStore {
  /** 更新在线数量 */
  updateOnline: (count: number) => void;
  /** 获取 store 在线数量 */
  getOnline: () => number;
  /** 获取通知缓存 */
  getNotices: () => NoticesTypeArray;
  /** 保存通知缓存*/
  saveNotices: (notices: any) => void;
}

type NoticesTypeArray = {
  allNoticeMessages: NoticeMessage[]
  systemNoticeMessages: NoticeMessage[]
  chatNoticeMessages: NoticeMessage[]
  detailNoticeMessages: NoticeMessage[]
}

export default class SocketMessageStoreImpl implements SocketMessageStore {
  namespace = new StoreNamespace('Socket');

  /**
   * 更新在线用户人数
   * @param count 人数数量
   */
  updateOnline(count: number) {
    this.namespace.dispatch(KEY.DISPATCH_SAVE_CURRENT_ONLINE, count);
  }

  /** 获取 store 在线数量 */
  getOnline(): number {
    return this.namespace.getters(KEY.GETTER_ONLINE_KEY);
  }

  /**
   * 保存通知缓存
   * @param notices notices: {allNoticeMessages, systemNoticeMessages, chatNoticeMessages, detailNoticeMessages}
   */
  saveNotices(notices: any) {
    this.namespace.dispatch(KEY.DISPATCH_SAVE_NOTICES_KEY, notices);
  }

  /** 获取通知缓存*/
  getNotices(): NoticesTypeArray {
    return this.namespace.getters(KEY.GETTER_NOTICES_KEY);
  }

}
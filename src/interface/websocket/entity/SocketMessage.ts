/**
 * socket 数据类型
 */
export enum MessageTypes {
  HeartType = -3,
  UserLogOut = -2,
  UserLogin = -1,
  CommonChat = 0,
  PrivateChat = 1,
  ExitPrivateChat = 2,
  ExitCommonChat = 3,
  DetailNotice = 4,
  GuestBook = 5
}

/** 通知消息类型 */
export interface NoticeMessage {
  /** 通知名称 */
  nickname: string;
  /** 通知类型 */
  type: number;
  /** 通知类型 -- str */
  typeStr: string;
  /** 通知时间，如果消息发布过来时没有获取到，那么就生成当前时间 */
  noticeTime: string;
  /** 通知内容 */
  content?: string;
  /** 内容id，一般作用于连接跳转 */
  id?: number;
  /** 通知头像 */
  avatar?: string;
  /** 针对文章的通知时，title 的处理 */
  title?: string;
}

/**
 * socket message 对象
 */
export interface Message {
  type: -3 | -2 | -1 | 0 | 1 | 2 | 3 | 4 | 5
  context?: any
  noticeTime?: string
  timestamp?: number
}


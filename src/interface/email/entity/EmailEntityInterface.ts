export interface BasicEmail {
    /** 时间戳 new Date().getTime() */
    timestamp: string,
    /** 邮箱字符串 */
    email: string
}
const emailReg:RegExp = new RegExp(/^(([^<>()[].,;:s@"]+(.[^<>()[].,;:s@"]+)*)|(".+"))@(([[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}])|(([a-zA-Z-0-9]+.)+[a-zA-Z]{2,}))$/);
export class BasicEmailBuilder implements BasicEmail {
    timestamp: string;
    email: string;

    constructor(basicEmail: BasicEmail) {
        this.timestamp = basicEmail.timestamp;
        if (emailReg.test(basicEmail.email)) {
            this.email = basicEmail.email;
        } else {
            throw Error('电子邮箱格式不正确');
        }
    }
}
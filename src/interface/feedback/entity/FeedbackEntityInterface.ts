/** 反馈实体接口*/

/** 反馈信息响应数据类型 */
export interface FeedbackResponseEntity {
  /** 用户名 */
  username: string
  /** 用户头像 */
  avatar: string
  /** 反馈日期 */
  date: string
  /** 反馈标题 */
  title: string
  /** 反馈内容 */
  content: string
}

/** 提交反馈信息 */
export interface FeedbackCommitEntity {
  /** 反馈标题 */
  title: string
  /** 反馈内容 */
  content: string
}
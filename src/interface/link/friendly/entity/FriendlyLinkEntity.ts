export interface FriendlyLinkEntity {
    linkSrc: string
    linkUrl: string
    linkName: string
}
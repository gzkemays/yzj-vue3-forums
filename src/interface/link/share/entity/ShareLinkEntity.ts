/** 传入接口的数据对象 */
export interface ShareLinkEntity {
  /** 链接名称 */
  linkName: string
  /** 链接图标 */
  linkSrc: string
  /** 链接地址 */
  linkUrl: string
  /** 链接标签 */
  linkTags: string
  /** 链接介绍 */
  linkDesc: string
  /** 分享用户 */
  nickname: string
}
/** 响应接口的数据对象 */
export interface ShareLinkResponse {
  linkName: string,
  linkUrl: string,
  linkSrc: string,
  linkTags: string,
  linkDesc: string,
  nickname: string,
  collect: 0
}
/**
 * 提交提问的信息
 */
import {UserMsg} from '@/interface/user/entity/UserEntityInterface';

export interface QuestionInfo {
  title: string
  content: string
}

/**
 * 提问的响应数据
 */
export interface QuestionResponse {
  id: number
  title: string
  content: string
  viewNumber: number
  collectionNumber: number
  answerNumber: number
  createTime: string
  updateTime: string
  // nickname: string
  author: UserMsg
  resolve: boolean
  toOpera: boolean
}

/**
 * question 分页
 */
export interface QuestionPaging {
  content: string
  page: number
  limit: number
}
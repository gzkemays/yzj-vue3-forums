import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
/** utils ts */
import Cache from './utils/Cache';
import Request from './utils/Request';
import toast from '@/utils/Toast';
/** animate css */
import 'animate.css';
/** hover css */
import 'hover.css';
/** primevue config */
import 'primevue/resources/primevue.min.css';
import 'primevue/resources/themes/lara-light-purple/theme.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import PrimeVue from 'primevue/config';
import ToastService from 'primevue/toastservice';
import ConfirmationService from 'primevue/confirmationservice';
import BadgeDirective from 'primevue/badgedirective';
import ScrollTop from 'primevue/scrolltop';
/** primevue components */
import Button from 'primevue/button';
import Avatar from 'primevue/avatar';
import Image from 'primevue/image';
import InputText from 'primevue/inputtext';
import Tooltip from 'primevue/tooltip';
import Textarea from 'primevue/textarea';
/** vditor css */
import 'vditor/dist/index.css';


const app = createApp(App), cache = new Cache();
// 加载组件
app.config.globalProperties.$cache = cache;
app.config.globalProperties.$rq = Request;
// 挂载服务
app.use(store).use(router).use(PrimeVue).use(ToastService).use(ConfirmationService).mount('#app');
// 导入组件
app.component('Button', Button);
app.component('Avatar', Avatar);
app.component('Image', Image);
app.component('InputText', InputText);
app.component('Textarea', Textarea);
app.component('ScrollTop', ScrollTop);
app.directive('tooltip', Tooltip);
app.directive('badge', BadgeDirective);
// 路由判断权限跳转
router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    // @ts-ignore
    window.document.title = to.meta.title;
  }
  if (to.matched.some(record => record.meta.requiredLogin)) {
    // 利用 storage 中是否存在 userMsg 作为判断
    if (cache.get('userMsg') && cache.get('token')) {
      next();
    } else {
      toast.warn("您未登录哦", "请先登录后重试(●'◡'●)", 3000);
      next({
        path: '/',
      })
    }
  } else {
    next();
  }
});
export default app;

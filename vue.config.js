const {
  defineConfig
} = require('@vue/cli-service');
const webpack = require('webpack');
const CompressionWebpackPlugin = require('compression-webpack-plugin');

require('events').EventEmitter.defaultMaxListeners = 0;
module.exports = defineConfig({
  publicPath: process.env.NODE_ENV === 'production' ? './' : '/',
  outputDir: 'dist',
  indexPath: 'index.html',
  transpileDependencies: true,
  lintOnSave: false,
  // 开发环境配置
  devServer: {
    port: 7000,
    proxy: {
      '/sp0': {
        target: 'http://yeziji.xyz/sp0',
        changeOrigin: true,
        pathRewrite: {
          '^/sp0': ''
        }
      },
      '/auth': {
        target: 'http://open.yeziji.xyz/auth',
        changeOrigin: true,
        pathRewrite: {
          '^/auth': ''
        }
      },
      '/openapi': {
        target: 'http://open.yeziji.xyz/openapi',
        changeOrigin: true,
        pathRewrite: {
          '^/openapi': ''
        }
      },
      "/forum": {
        // target: "https://yeziji.xyz/forum",
        target: process.env.NODE_ENV === "development" ? 'http://127.0.0.1:9998/forum' : 'https://yeziji.xyz/forum',
        changeOrigin: true,
        pathRewrite: {
          "^/forum": "/" //路径重写，将接口路径中以/forum开头的部分替换掉
        }
      },
      "/file": {
        // target: "https://yeziji.xyz/file",
        target: process.env.NODE_ENV === "development" ? 'http://127.0.0.1:9999/file' : 'https://yeziji.xyz/forum',
        changeOrigin: true,
        pathRewrite: {
          "^/file": "/" //路径重写，将接口路径中以/file开头的部分替换掉
        }
      },
      "/open": {
        target: "http://open.yeziji.xyz",
        changeOrigin: true,
        pathRewrite: {
          "^/open": "/" //路径重写，将接口路径中以/api开头的部分替换掉
        }
      }
    },
    open: true,
  },
  configureWebpack: config => {
    config.optimization = {
      splitChunks: {
        chunks: 'all',
        cacheGroups: {
          'primevue': {
            name: 'primevue',
            test: /[\\/]primevue[\\/]/,
            priority: 10
          },
          'vditor': {
            name: 'vditor',
            test: /[\\/]@vditor[\\/]/,
            priority: 10
          },
          'vue-pdf-embed': {
            name: 'vue-pdf-embed',
            test: /[\\/]vue-pdf-embed[\\/]/,
            priority: 10
          },
          'pdfjs-dist': {
            name: 'pdfjs-dist',
            test: /[\\/]pdfjs-dist[\\/]/,
            priority: 10
          },
        }
      }
    }
    //以下配置打开项目gzip打包
    config.plugins.push(
            new webpack.ProvidePlugin({
              $: "jquery",
              jQuery: "jquery",
            }),
            new CompressionWebpackPlugin({
              algorithm: 'gzip', // 使用gzip压缩
              test: /\.js$|\.html$|\.css$/, // 匹配文件名
              filename: '[path].gz[query]', // 压缩后的文件名(保持原文件名，后缀加.gz)
              minRatio: 1, // 压缩率小于1才会压缩
              threshold: 10240, // 对超过10k的数据压缩
              deleteOriginalAssets: false, // 是否删除未压缩的源文件，谨慎设置，如果希望提供非gzip的资源，可不设置或者设置为false（比如删除打包后的gz后还可以加载到原始资源文件）
            }))
  },
})